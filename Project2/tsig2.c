#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int sigint_interrputs = 0;

void sig_handler(int signo)
{
	sigint_interrputs++;
	printf("\nParent[%d]: SIGINT signal was received. It has been received %d times so far\n", getpid(), sigint_interrputs);

}

void sig_handler_c(int signo)
{
	printf("Child[%d]: I am terminating\n", getpid());
	
	exit(0);
}

int main()
{
	int NUM_CHILD = 5;
	int pids[NUM_CHILD];	//I will store pids of the children here
	int p_counter = 0;
	int pid;
	
	//a place to handle the signals as parent
	for(int i=0; i<32; i++)
		signal(i, SIG_IGN);
	signal(SIGCHLD, SIG_DFL);	//restore SIGCHLD handling
	signal(SIGINT, sig_handler);
	
	for(int i=0;i<NUM_CHILD;i++)
	{
		pid = fork();	//pid = 0 if in the child, otherwise it's the pid of the child
		//if(i==2)
		//	pid = -1;
		if(pid==-1)	//child process was not created
		{
			printf("Parent[%d]: A child could not be created, terminating\n", getpid());
			for(int j = 0; j<p_counter; j++)
				kill(pids[j], SIGTERM);
			return 1;
		}
		
		pids[p_counter] = pid;	//store the pid of the new child
		p_counter++;
		
		
		if(pid == 0)		//we are in the child and we want to do something
		{
			printf("Child[%d]: I have just been created, my parent pid %d\n",getpid(), getppid());
			
			//a place to handle the signals as child
			for(int i=0; i<32 && i!=SIGTERM; i++)
				signal(i, SIG_IGN);
			signal(SIGTERM, sig_handler_c);
			sleep(10);
			printf("Child[%d]: My task has just been finished\n", getpid());
			exit(0);	//child is done doing its task
		}
		sleep(1);
		
		if(sigint_interrputs>0 && pid)
		{
			printf("Parent[%d]: SIGINT signal has been sent while creating children\n", getpid());
			for(int j = 0; j<p_counter; j++)
				kill(pids[j], SIGTERM);
			i = NUM_CHILD+1;
		}
	}
	if(pid)
	{
		int s_counter = 0;
		for(int i=0; i<NUM_CHILD; i++)	//wait for all children to send exit signal
		{
			if(wait(NULL) != -1)
				s_counter++;		//count how many children processes ended successfully
		}
		printf("Parent[%d]: All child processes have finished their tasks\n", getpid());
		
		printf("The number of successfully ended child processes %d\n", s_counter);
		
		for(int i=0; i<32; i++)
			signal(i, SIG_DFL);
	}
	
	return 0;
}
