#!/bin/bash

function lc	#change to lowercase
{
	file_name="$1"		#remember the name of the changed file
	ext="${file_name#*.}"	#extract the extension for preserving it
	file_name=${file_name/.$ext/}	#get just the name without the extension
	b=$(echo "$file_name" | sed -e 's/\(.*\)/\L\1/')	#use sed pattern to change to lowercase
	b=$b.$ext		#give back the extension
}

function uc	#change to uppercase
{
	file_name="$1"		#remember the name of the changed file
	ext="${file_name#*.}"	#extract the extension for preserving it
	file_name=${file_name/.$ext/}	#get just the name without the extension
	b=$(echo "$file_name" | sed -e 's/\(.*\)/\U\1/')	#use sed pattern to change to lowercase
	b=$b.$ext		#give back the extension
}

function open_all_in_all
{
	for file in *;	#do something for every file in directory you are in
	do
		cd $1	#we passed the pwd to continue where we last finished
		for (( i=0;i<${#file_names[@]};i++)); do	#for every file matching the name there is in this directory
			if test $u = "y" && test -f ${file_names[$i]}; then	#uppercase it when -u was set, also check if is a file
				uc ${file_names[$i]}
				mv ${file_names[$i]} $b					#change the name
			fi
			if test $l = "y" && test -f ${file_names[$i]}; then	#lowercase it when -u was set, also check if is a file
				lc ${file_names[$i]}
				mv ${file_names[$i]} $b					#change the name
			fi
			if test $l = "n" && test $l = "n" && test $sed_pattern != "n" && test -f ${file_names[$i]}; then	#if we do not uppercase or lowercase then we have a sed pattern
				sed -n $sed_pattern ${file_names[$i]}
				if test $? = 0; then	#check if last command was executed successfully
					b=$(echo ${file_names[$i]} | sed -e $sed_pattern)	#use the sed pattern on file
					mv ${file_names[$i]} $b				#change the name
				fi
			fi
		done
		if [ -d $file ]; then	#if some file is a directory then go inside and call the function to change file names inside
			cd $file
			open_all_in_all `pwd`
		fi
	done
}
function show_help
{
cat<<EOT 1>&2
The script can lowerize or capitalize file names. The following syntax applies:

usage:
  modify [-l|-u] <dir/file names>	will lowerize (-l) or capitalize (-u) passed filenames in a directory, for example
  
  modify -u /home/(...)/{ayx.bb,zxy.aa}	will go to provided directory and change file names from ayx.bb and zxy.aa to AYX.bb and ZXY.aa
  modify -l /home/(...)/{AYX.bb,ZXY.aa}	will go to provided directory and change file names from AYX.bb and ZXY.aa to ayx.bb and zxy.aa
  modify -u -r /home/(...)/{ayx.bb,zxy.aa}	will go to provided directory and recursively change file names like previousely
  modify 's/x/d/g' /home/(...)/zxy.aa		will go to provided directory and change file name accordingly to the sed pattern, from zxy.aa to zdy.aa
  modify -r -u /home/(...)/some_dir/*		will uppercase all file names in all directories within some_dir
  modify -h	will echo this help


incorrect syntax examples: 
  modify -u -r					will echo a message that the path is incorrect
  modify -d /home/(...)zxy.aa			will recognize invalid flag and terminate
  modify 's/d/a' /home/(...)zxy.aa		will try to apply the sed pattern, but it will fail as the pattern is invalid
  modify -l ZXY.aa				will not modiify the name path was not specified, the same applies if the file is in the same directory as the script
EOT
}

#let us get options, path and file names
r=n
l=n
u=n
path=()
count=0
sed_pattern=n
while test "x$1" != "x"
do
	set_sed=n	#we did not set anything yet so let's prepare to accept sed pattern
        case "$1" in
                -r|[-r]) r=y set_sed=y;;		#check what flags were given
                -u|[-u]) u=y set_sed=y;;
                -l|[-l]) l=y set_sed=y;;
                -h|[-h]) show_help; exit 0;;
                -*|[-*]) echo "Invalid flag"; exit 1;;
        esac
        #get the file path and names
        probably_path=$1
        if [[ -d ${probably_path%/*} ]]; then		#check if is a valid directory and save it as path
        	path[$count]=$1
        	((count++))
 		set_sed=y
        fi
        #if no uppercasing or lowercasing then we expect a sed pattern
        if test "$set_sed" = "n"; then
        	sed_pattern=$1
        fi
        shift
done

if [ -z "$path" ]; then		#if the path was not set previousely then we do not know what we should do
	echo "Path is incorrect or the files do not exist"
	exit
fi

cd ${path[0]%/*}	#now we are in the directory we wanted to modify files in

#we need to store the file names for convenience
file_names=()
for (( i=0;i<${#path[@]};i++)); do
    file_names[$i]="$(basename -- ${path[${i}]})"	#here we extract file names from a full path and store them in a array
done

cd ${path[0]%/*}	#Go to the directory to make some changes

if test $r = "n"; then	#we do not do it recursively so no function needed
	for (( i=0;i<${#file_names[@]};i++)); do	#browse the saved file names to find what we are interested in
		if test $u = "y" && test -f ${file_names[$i]}; then	#uppercase what needed
			uc ${file_names[$i]}
			mv ${file_names[$i]} $b			#change the name
		fi
		if test $l = "y" && test -f ${file_names[$i]}; then	#lowercase what needed
			lc ${file_names[$i]}
			mv ${file_names[$i]} $b			#change the name
		fi
		if test $l = "n" && test $l = "n" && test $sed_pattern != "n" && test -f ${file_names[$i]}; then
			sed -n $sed_pattern ${file_names[$i]}
			if test $? = 0; then				#see if the sed pattern executed properly
				b=$(echo ${file_names[$i]} | sed -e $sed_pattern)	#apply the sed pattern
				echo $b
				mv ${file_names[$i]} $b		#change the name
			fi
		fi
	done
else
	open_all_in_all `pwd`						#do the same thing as above, just recursively 
fi
