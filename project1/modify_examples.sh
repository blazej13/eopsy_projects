cat<<EOT 1>&2
The script can lowerize or capitalize file names. The following syntax applies:

usage:
  modify [-l|-u] <dir/file names>	will lowerize (-l) or capitalize (-u) passed filenames in a directory, for example
  
  modify -u /home/(...)/{ayx.bb,zxy.aa}	will go to provided directory and change file names from ayx.bb and zxy.aa to AYX.bb and ZXY.aa
  modify -l /home/(...)/{AYX.bb,ZXY.aa}	will go to provided directory and change file names from AYX.bb and ZXY.aa to ayx.bb and zxy.aa
  modify -u -r /home/(...)/{ayx.bb,zxy.aa}	will go to provided directory and recursively change file names like previousely
  modify 's/x/d/g' /home/(...)/zxy.aa		will go to provided directory and change file name accordingly to the sed pattern, from zxy.aa to zdy.aa
  modify -r -u /home/(...)/some_dir/*		will uppercase all file names in all directories within some_dir
  modify -h	will echo this help


incorrect syntax examples: 
  modify -u -r					will echo a message that the path is incorrect
  modify -d /home/(...)zxy.aa			will recognize invalid flag and terminate
  modify 's/d/a' /home/(...)zxy.aa		will try to apply the sed pattern, but it will fail as the pattern is invalid
  modify -l ZXY.aa				will not modiify the name path was not specified, the same applies if the file is in the same directory as the script
EOT
